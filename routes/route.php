<?php
    require_once '../app/Controllers/StringController.php';

    // ajax server side handler -- sort the given string depending on the chosen strategy
    if (isset($_POST['sort-type'])) {
        $strCtrlr = new StringController($_POST['string']);
        $type = $_POST['sort-type'];
        $result = '';
        
        if ($type == 'quick') {
            $result = $strCtrlr->quickSort();
        } else {
            $result = $strCtrlr->bubbleSort();
        }

        echo json_encode(implode($result));
        return;
    }
