<?php

class StringController
{
    private $strArray = [];
    private $strCounter = 0;

    /**
     * Assign parameters to private variables.
     * @param  [string] $string - string to be sorted.
     */
    public function __construct($string)
    {
        $this->strArray = str_split($string);
        $this->strCounter = sizeof($this->strArray);
    }

    /**
     * Sort string using quick sort strategy.
     * @return [array] $string - sorted string.
     */
    public function quickSort() {
        $strArray = $this->strArray;
        $this->quickSortSub($strArray, 0, $this->strCounter - 1);

        return $strArray;
    }

    /**
     * Sort string using quick sort strategy.
     * @param  [string] $category - category name.
     * @param  [int] $left - left value from string array.
     * @param  [int] $right - right value from string array.
     */
    function quickSortSub(&$strArray, $left, $right) {
        if ($left < $right) { 
            $pivot = $this->partition($strArray, $left, $right);
            $this->quickSortSub($strArray, $left, $pivot-1);
            $this->quickSortSub($strArray, $pivot+1, $right);
        }
    }

    /**
     * Arrange array into two lists.
     * @param  [array] $string - string to be sorted.
     * @param  [int] $left - left value from string array.
     * @param  [int] $right - right value from string array.
     * @return [int] $left - left value from string array.
     */
    function partition(&$strArray, $left, $right) {
        $i = $left;
        $pivot = $strArray[$right];
        for($j = $left; $j <=$right; $j++) {
          if($strArray[$j] < $pivot) {
            $temp = $strArray[$i];
            $strArray[$i] = $strArray[$j];
            $strArray[$j] = $temp;
            $i++;
          }
        }
      
        $temp = $strArray[$right];
        $strArray[$right] = $strArray[$i];
        $strArray[$i] = $temp;
        return $i;
    }

    /**
     * Sort string using bubble sort strategy.
     * @return [array] $string - sorted string.
     */
    public function bubbleSort() {
        $str = $this->strArray;
        $strSorted;

        for ($i = 0; $i < $this->strCounter; $i++) {
            for ($j = 0; $j < $this->strCounter - $i - 1; $j++) {
                if (strcmp($str[$j], $str[$j+1]) > 0) {
                    $strSorted = $str[$j];
                    $str[$j] = $str[$j+1];
                    $str[$j+1] = $strSorted;
                }
            }
        }
        
        return $str;
    }
}