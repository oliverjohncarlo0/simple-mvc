<a name="readme-top"></a>

<div align="center">
    <h3 align="center">Programming Assignment</h3>
</div>

<!-- ABOUT THE TEST -->
## About the test

Create a PHP application that has the following requirements.
The program's primary functionality is to sort a string with the input from a user.
Example input: befdac
Expected output: abcdef
The user interface will contain the following four elements:
1. A textbox that allows the user to input a string
2. A control (for instance a combobox) that selects the strategy to use for sorting the string.
3. A button that will sort the string using the selected strategy.
4. A label where the output will be shown (the sorted string)

## Applicant Information

### JOHN CARLO OLIVER

_Start Date: January 29, 2023 @ 4:30 PM_

_End Date: January 21, 2023 @  8:30 PM_