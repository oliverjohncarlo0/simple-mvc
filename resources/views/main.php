<?php  require_once 'partials/header.php'; ?> 

<div class="vh-100">
    <div class="container card-container">
        <div class="card shadow-lg overflow-hidden m-0">
            <div class="card-body">
                <h3 class="text-title mb-1">Try sorting!</h3>
                <p class="text-muted w-75 mb-4">Input any string that you'd like to sort alphabetically.</p>

                <form id="sorting-form">
                    <div class="input-group">
                        <select id="sort-type" name="sort-type" class="form-select">
                            <option value="quick" selected>Quick Sort</option>
                            <option value="bubble">Bubble Sort</option>
                        </select>
                        <input id="string" type="text" name="string" class="form-control" placeholder="Input string here..." required>
                        <button class="btn btn-success fw-bold px-3" type="submit">SORT</button>
                    </div>
                </form>

                <div class="mt-3 p-2 bg-light rounded">
                    <div class="border-dash p-2 text-center rounded">
                        <h3 id="sorted-text" class="m-0">result</h3>
                    </div>
                </div>

                <footer class="footer footer-alt mt-5">
                    <div class="row">
                        <div class="col-6 col-xl-3 my-auto">
                            <img src="https://uploads-ssl.webflow.com/61842bed146f31063a492805/61b28dfeb83d073049c19ad1_Group%2024.svg" alt="" height="35">
                        </div>
                        <div class="col-6 col-xl-9 text-end my-auto">
                            <script>document.write(new Date().getFullYear())</script> © Powered by <a href="https://www.you-source.com/" target="_blank" class="text-theme text-decoration-underline fw-medium">You_Source</a>
                            <p class="text-muted small m-0">Developed by <strong>JOHN CARLO OLIVER</strong></p>
                        </div>
                    </div>
                </footer>
            </div> <!-- end .card-body  -->
        </div> <!-- end .card  -->
    </div> <!-- end .container  -->
</div>

<?php  require_once 'partials/footer.php'; ?> 