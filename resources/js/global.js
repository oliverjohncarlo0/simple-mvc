$(function(e) {
    // perform ajax request upon form submission
    $('#sorting-form').on('submit', function(e) {
        e.preventDefault();
        console.log($(this).serializeArray());

        $.ajax({
            type: 'POST',
            url: 'routes/route.php',
            data: $(this).serializeArray(),
            dataType: 'json',
            success: function(response) {
                $('#sorted-text').text(response);
            },
            error: function(error) {
                console.log(error);
            }
        });
    })
})